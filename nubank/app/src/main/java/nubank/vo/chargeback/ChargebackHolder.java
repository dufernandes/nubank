package nubank.vo.chargeback;


public class ChargebackHolder {
    private String autoblock;

    private String id;

    private String title;

    private String comment_hint;

    private Links links;

    private Reason_details[] reason_details;

    public String getAutoblock ()
    {
        return autoblock;
    }

    public void setAutoblock (String autoblock)
    {
        this.autoblock = autoblock;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getComment_hint ()
    {
        return comment_hint;
    }

    public void setComment_hint (String comment_hint)
    {
        this.comment_hint = comment_hint;
    }

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }

    public Reason_details[] getReason_details ()
    {
        return reason_details;
    }

    public void setReason_details (Reason_details[] reason_details)
    {
        this.reason_details = reason_details;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [autoblock = "+autoblock+", id = "+id+", title = "+title+", comment_hint = "+comment_hint+", links = "+links+", reason_details = "+reason_details+"]";
    }
}
