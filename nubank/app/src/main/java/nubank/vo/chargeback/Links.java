package nubank.vo.chargeback;

public class Links {
    private Unblock_card unblock_card;

    private Self self;

    private Block_card block_card;

    public Unblock_card getUnblock_card ()
    {
        return unblock_card;
    }

    public void setUnblock_card (Unblock_card unblock_card)
    {
        this.unblock_card = unblock_card;
    }

    public Self getSelf ()
    {
        return self;
    }

    public void setSelf (Self self)
    {
        this.self = self;
    }

    public Block_card getBlock_card ()
    {
        return block_card;
    }

    public void setBlock_card (Block_card block_card)
    {
        this.block_card = block_card;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [unblock_card = "+unblock_card+", self = "+self+", block_card = "+block_card+"]";
    }
}
