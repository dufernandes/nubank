package nubank.vo.chargebackpost;

public class Reason_details {
    private String response;

    private String id;

    public String getResponse ()
    {
        return response;
    }

    public void setResponse (String response)
    {
        this.response = response;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [response = "+response+", id = "+id+"]";
    }
}
