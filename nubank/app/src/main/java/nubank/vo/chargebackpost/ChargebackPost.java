package nubank.vo.chargebackpost;

public class ChargebackPost {
    private String comment;

    private Reason_details[] reason_details;

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public Reason_details[] getReason_details ()
    {
        return reason_details;
    }

    public void setReason_details (Reason_details[] reason_details)
    {
        this.reason_details = reason_details;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [comment = "+comment+", reason_details = "+reason_details+"]";
    }
}
