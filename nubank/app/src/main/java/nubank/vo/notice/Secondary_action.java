package nubank.vo.notice;

public class Secondary_action {
    private String title;

    private String action;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", action = "+action+"]";
    }
}
