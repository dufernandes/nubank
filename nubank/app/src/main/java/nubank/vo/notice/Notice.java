package nubank.vo.notice;


public class Notice {

    private String title;

    private Secondary_action secondary_action;

    private String description;

    private Primary_action primary_action;

    private Links links;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public Secondary_action getSecondary_action ()
    {
        return secondary_action;
    }

    public void setSecondary_action (Secondary_action secondary_action)
    {
        this.secondary_action = secondary_action;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Primary_action getPrimary_action ()
    {
        return primary_action;
    }

    public void setPrimary_action (Primary_action primary_action)
    {
        this.primary_action = primary_action;
    }

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", secondary_action = "+secondary_action+", description = "+description+", primary_action = "+primary_action+", links = "+links+"]";
    }
}
