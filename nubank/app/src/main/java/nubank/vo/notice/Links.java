package nubank.vo.notice;

public class Links {
    private Chargeback chargeback;

    public Chargeback getChargeback ()
    {
        return chargeback;
    }

    public void setChargeback (Chargeback chargeback)
    {
        this.chargeback = chargeback;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [chargeback = "+chargeback+"]";
    }
}
