package nubank.vo.root;

public class Links {
    private Notice notice;

    public Notice getNotice ()
    {
        return notice;
    }

    public void setNotice (Notice notice)
    {
        this.notice = notice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [notice = "+notice+"]";
    }
}
