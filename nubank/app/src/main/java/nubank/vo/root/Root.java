package nubank.vo.root;

public class Root {
    private Links links;

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [links = "+links+"]";
    }
}
