package nubank.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import nubank.activity.R;

/**
 * This class holds utilitary methods for android UI usage
 */
public class ActivityUtil {

    /**
     * Show an unexpected error message to the user.
     *
     * @param context Context to associate the message.
     */
    public static void showUnexpectedErrorMessage(Context context) {
        Toast.makeText(context, R.string.msg_unexpected_error, Toast.LENGTH_LONG).show();
    }

    /**
     * Return a new View.OnClickListener for a certain activity, which closes the
     * application.
     *
     * @param activity Activity associated with the action of closing the application.
     *
     * @return View.OnClickListener which closes the application.
     */
    public static View.OnClickListener createCloseApplicatonViewOnClickListener(Activity activity) {
        final Activity innerActivity = activity;
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.finishAffinity(innerActivity);
            }
        };
    }

    /**
     * Show the standard dialog for loading data.
     *
     * @param context Context which the dialog will be displayed.
     *
     * @return ProgressDialog to be displayed.
     */
    public static ProgressDialog showDialogForLoading(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(R.string.title_Loading);
        progressDialog.setMessage(context.getResources().getText(R.string.msg_loading_data));
        progressDialog.show();

        return progressDialog;
    }

    /**
     * Close a Progress ProgressDialog.
     *
     * @param dialog ProgressDialog to be closed.
     */
    public static void closeDialog(ProgressDialog dialog) {
        dialog.dismiss();
    }
}
