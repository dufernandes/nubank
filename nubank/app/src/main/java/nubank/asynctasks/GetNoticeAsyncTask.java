package nubank.asynctasks;


import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

import nubank.vo.notice.Notice;

public class GetNoticeAsyncTask extends AsyncTaskLoadingMsg<String, Integer, Notice> {

    private static final String LOG_TAG = GetNoticeAsyncTask.class.getName();

    private WeakReference<PostExecution<Notice>> postExec = null;

    public GetNoticeAsyncTask(Context context, ProgressDialog progressDialog, PostExecution<Notice> postExec) {
        super(context, progressDialog);
        this.postExec = new WeakReference<PostExecution<Notice>>(postExec);
    }

    @Override
    protected Notice doInBackground(String... params) {

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the Jackson message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Notice notice = null;
        try {
            notice = restTemplate.getForObject(params[0], Notice.class);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Unexpected error", ex);
        }


        return notice;
    }

    @Override
    protected void executePostExecuteAction(Notice notice) {
        if (postExec != null && postExec.get() != null ) {
            postExec.get().populateUI(notice);
        }
    }
}
