package nubank.asynctasks;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

import nubank.vo.chargeback.ChargebackHolder;

public class GetChargebackAsyncTask extends AsyncTaskLoadingMsg<String, Integer, ChargebackHolder> {

    private static final String LOG_TAG = GetChargebackAsyncTask.class.getName();

    private WeakReference<PostExecution<ChargebackHolder>> postExec = null;

    public GetChargebackAsyncTask(Context context, PostExecution<ChargebackHolder> postExec) {
        super(context);
        this.postExec = new WeakReference<PostExecution<ChargebackHolder>>(postExec);
    }

    @Override
    protected ChargebackHolder doInBackground(String... params) {

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the Jackson message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        ChargebackHolder chargebackHolder = null;
        try {
            chargebackHolder = restTemplate.getForObject(params[0], ChargebackHolder.class);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Unexpected error", ex);
        }


        return chargebackHolder;
    }

    @Override
    protected void executePostExecuteAction(ChargebackHolder chargebackHolder) {
        if (postExec != null && postExec.get() != null ) {
            postExec.get().populateUI(chargebackHolder);
        }
    }
}
