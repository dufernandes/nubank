package nubank.asynctasks;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

import nubank.activity.R;
import nubank.util.ActivityUtil;
import nubank.vo.chargebackpost.ChargebackPost;

/**
 * This task execute a HTTP POST sending data for refuting a credit card purchase.
 */
public class PostChargebackAsyncTask extends AsyncTaskLoadingMsg<String, Integer, Boolean> {

    private static final String LOG_TAG = PostChargebackAsyncTask.class.getName();

    private WeakReference<ChargebackPost> chargebackPost = null;

    public PostChargebackAsyncTask(Activity activity, ChargebackPost chargebackPost) {
        super(activity);
        this.chargebackPost = new WeakReference<ChargebackPost>(chargebackPost);
    }

    @Override
    protected Boolean doInBackground(String... params) {

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the Jackson message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Boolean success = false;
        try {
            if (chargebackPost != null) {
                ResponseEntity<String> response = restTemplate.postForEntity(params[0], chargebackPost.get(), String.class);
                success = response.getStatusCode().is2xxSuccessful();
            }
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Unexpected error", ex);
        }


        return success;
    }


    /**
     * Here is the refutal operation is successfully done, a dialog is displayed
     * informing the user he has to wait for a few days in order to get a response.
     *
     * @param successOperation Result of the refutal operation - true the operation
     *                         was successfully done, false, an error has occurred.
     */
    @Override
    protected void executePostExecuteAction(Boolean successOperation) {
        WeakReference<Context> contextWR = this.getContextWR();
        if (contextWR != null && contextWR.get() != null) {
            final Activity activity = (Activity) contextWR.get();
            if (successOperation) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                // inflate layout programmatically
                LayoutInflater inflater = activity.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_contestation, null);

                // add termination listener
                Button closeBtn = (Button) dialogView.findViewById(R.id.btnClose);
                closeBtn.setOnClickListener(ActivityUtil.createCloseApplicatonViewOnClickListener(activity));

                // attatch dialog to view
                builder.setView(dialogView);

                // create and display dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // show error message due to a failure in the HTTP request
                Log.e(LOG_TAG, "An error has occured while posting a Chargeback");
                ActivityUtil.showUnexpectedErrorMessage(activity);
            }
        }
    }
}
