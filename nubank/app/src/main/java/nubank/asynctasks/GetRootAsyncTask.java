package nubank.asynctasks;


import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

import nubank.util.ActivityUtil;
import nubank.vo.notice.Notice;
import nubank.vo.root.Root;

/**
 * This task fetches the data needed to populate the initial UI of this application.
 */
public class GetRootAsyncTask extends AsyncTaskLoadingMsg<Void, Integer, Root> {

    private static final String LOG_TAG = GetRootAsyncTask.class.getName();

    private WeakReference<PostExecution<Notice>> postExec = null;

    public GetRootAsyncTask(Context context, PostExecution<Notice> postExec) {
        super(context);
        this.postExec = new WeakReference<PostExecution<Notice>>(postExec);
    }

    @Override
    protected Root doInBackground(Void... params) {

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the Jackson message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Root root = null;
        try {
            root = restTemplate.getForObject("https://nu-mobile-hiring.herokuapp.com/", Root.class);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Unexpected error", ex);
        }


        return root;
    }

    @Override
    protected void onPostExecute(Root root) {
        // get notice data to fill in screen
        if (root != null && postExec.get() != null && this.getProgressDialogWR().get() != null) {
            GetNoticeAsyncTask getChartBackTask = new GetNoticeAsyncTask(getContextWR().get(), this.getProgressDialogWR().get(), postExec.get());
            getChartBackTask.execute(root.getLinks().getNotice().getHref());
        } else {

            // problem going through the normal flow, close progress and show error message
            Log.e(LOG_TAG, "Either the request went wrong or the WeakReference to the PostExecution is null");
            if (this.getProgressDialogWR() != null) {
                // clos progress dialog
                ActivityUtil.closeDialog(this.getProgressDialogWR().get());
                if (this.getContextWR().get() != null) {
                    // show error message
                    Log.i(LOG_TAG, "Attempting to show error message.");
                    Context context = this.getContextWR().get();
                    ActivityUtil.showUnexpectedErrorMessage(context);
                    Log.i(LOG_TAG, "Error message displayed successfully");
                }
            }
        }
    }

    @Override
    protected void executePostExecuteAction(Root root) {
        // do nothing
    }
}
