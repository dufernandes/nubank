package nubank.asynctasks;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

import nubank.vo.chargeback.ChargebackHolder;

/**
 * This service execute a HTTP POST operation for blocking or unblocking a credit card.
 */
public class PostCardOperationAsyncTask extends AsyncTaskLoadingMsg<String, Integer, Boolean> {

    private static final String LOG_TAG = PostCardOperationAsyncTask.class.getName();

    private WeakReference<PostExecution<Boolean>> postExec = null;

    public PostCardOperationAsyncTask(Context context, PostExecution<Boolean> postExec) {
        super(context);
        this.postExec = new WeakReference<PostExecution<Boolean>>(postExec);
    }

    @Override
    protected Boolean doInBackground(String... params) {

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the Jackson message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Boolean success = null;
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(params[0], null, String.class);
            success = response.getStatusCode().is2xxSuccessful();
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Unexpected error", ex);
        }


        return success;
    }


    @Override
    protected void executePostExecuteAction(Boolean successOperation) {
        if (postExec != null && postExec.get() != null ) {
            postExec.get().populateUI(successOperation);
        }
    }
}
