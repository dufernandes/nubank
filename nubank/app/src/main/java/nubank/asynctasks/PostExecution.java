package nubank.asynctasks;

/**
 * Interface used to do any necessary action in the UI, after an AsyncTask is finished.
 * Its method populateUI is usually called on the AsyncTask method onPostExecute or on
 * the AsyncTaskLoadingMsg method executePostExecuteAction.
 */
public interface PostExecution<T> {

    /**
     * Populate an Activity UI.
     *
     * @param value Data used to fill in an Activity UI.
     */
    public void populateUI(T value);
}
