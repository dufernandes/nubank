package nubank.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import nubank.util.ActivityUtil;

/**
 * This ia a basic AsyntTask service which allows its children to inherit the
 * mechanism for displaying a dialog which asks for the user to wait a until
 * the serivce is finished.
 */
public abstract class AsyncTaskLoadingMsg<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private WeakReference<Context> contextWR = null;

    private WeakReference<ProgressDialog> progressDialogWR = null;

    /**
     * Default constructor which receives the context to attach the dialog.
     *
     * @param context Context which the dialog will be attached.
     */
    public AsyncTaskLoadingMsg(Context context) {
        this.contextWR = new WeakReference<Context>(context);
    }

    /**
     * In this alternative constructor the Dialog to be used is passes as a parameter.
     * Now this class will control this Dialog lifecycle.
     *
     * @param context Context which the dialog will be attached.
     * @param progressDialog Dialog to be displayed to the user.
     */
    public AsyncTaskLoadingMsg(Context context, ProgressDialog progressDialog) {
        this(context);
        this.progressDialogWR = new WeakReference<ProgressDialog>(progressDialog);
    }

    /**
     * Displays the dialog showing a message for the user to wait until the AsyncTaks
     * is finished.
     */
    @Override
    protected void onPreExecute() {
        // show progress dialog
        if (progressDialogWR == null && contextWR.get() != null) {
            progressDialogWR = new WeakReference<ProgressDialog>(ActivityUtil.showDialogForLoading(contextWR.get()));
        }
    }

    /**
     * Event executed after the AsyncTask has its job finished by the methdo doInBackground.
     * Here the Dialog is closed and a method to be implemented the the children of this
     * class will be executed. This method runs in the UI Thread and is usually needed
     * to perform UI operations.
     *
     * @param result Result of the AsyncTask execution.
     */
    @Override
    protected void onPostExecute(Result result) {
        executePostExecuteAction(result);
        if (progressDialogWR.get() != null) {
            ActivityUtil.closeDialog(progressDialogWR.get());
        }
    }

    /**
     * Method executed in the event onPostExecute in in the AsyncTak. This method
     * is run in the UI Thread.
     *
     * @param result Result of the AsyncTask doInBackground method.
     */
    protected abstract void executePostExecuteAction(Result result);

    protected WeakReference<Context> getContextWR() {
        return contextWR;
    }

    protected void setContextWR(WeakReference<Context> contextWR) {
        this.contextWR = contextWR;
    }

    protected WeakReference<ProgressDialog> getProgressDialogWR() {
        return progressDialogWR;
    }

    protected void setProgressDialogWR(WeakReference<ProgressDialog> progressDialogWR) {
        this.progressDialogWR = progressDialogWR;
    }
}
