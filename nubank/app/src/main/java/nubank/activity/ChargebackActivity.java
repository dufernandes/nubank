package nubank.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import nubank.asynctasks.GetChargebackAsyncTask;
import nubank.asynctasks.PostCardOperationAsyncTask;
import nubank.asynctasks.PostChargebackAsyncTask;
import nubank.asynctasks.PostExecution;
import nubank.util.ActivityUtil;
import nubank.vo.chargeback.ChargebackHolder;
import nubank.vo.chargebackpost.ChargebackPost;

/**
 * User interface which allows the user to select options to refute his or hers
 * credit card purchase.
 */
public class ChargebackActivity extends Activity {

    private static final String LOG_TAG = ChargebackActivity.class.getName();

    // indexes used to build potions for toggle elements
    private static final int REASON_MERCHANT_INDEX = 0;
    private static final int REASON_CARD_INDEX = 1;
    private static final int REASON_ARRAY_SIZE = 2;

    // reasons vector storing data comming from fetching Chargeback items
    private String[] mReasonsId = new String[REASON_ARRAY_SIZE];
    // Chargeback URL provided by the previous screen
    private String mChargebackURL = null;

    // Chargeback object used to fulfill this screen
    private ChargebackHolder chargeback = null;


    // UI components
    private LinearLayout mLilMain = null;
    private RelativeLayout mRelForm = null;
    private TextView mTxvTitle = null;
    private TextView mTxvCardStatus = null;
    private ToggleButton mTgbBlock = null;
    private TextView mTxvMerchant = null;
    private ToggleButton mTgbMerchant = null;
    private TextView mTxvCreditCard = null;
    private ToggleButton mTgbCreditCard = null;
    private EditText mEdtComments = null;
    private Button btnCancel = null;
    private Button mBtnRefute = null;

    // post executors
    private PostChargebackAutoBlockPostExecutor postChargeBackAutoBlockPostExec = new PostChargebackAutoBlockPostExecutor();
    private PostChargebackBlockPostExecutor postChargeBackBlockPostExec = new PostChargebackBlockPostExecutor();
    private PostChargebackUnblockPostExecutor postChargeBackUnblockPostExec = new PostChargebackUnblockPostExecutor();
    private GetChargeBackPostExecutor getChargePostExecutor = new GetChargeBackPostExecutor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargeback);

        // fetch UI components
        mLilMain = (LinearLayout) findViewById(R.id.lilMain);
        mRelForm = (RelativeLayout) findViewById(R.id.relForm);
        mTxvTitle = (TextView) findViewById(R.id.txvTitle);
        mTxvCardStatus = (TextView) findViewById(R.id.txvCardStatus);
        mTgbBlock = (ToggleButton) findViewById(R.id.tgbBlock);
        mTxvMerchant = (TextView) findViewById(R.id.txvMerchant);
        mTgbMerchant = (ToggleButton) findViewById(R.id.tgbMerchant);
        mTxvCreditCard = (TextView) findViewById(R.id.txvCreditCard);
        mTgbCreditCard = (ToggleButton) findViewById(R.id.tgbCreditCard);
        mEdtComments = (EditText) findViewById(R.id.txvComments);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        mBtnRefute = (Button) findViewById(R.id.btnRefute);

        // add listeners
        mTgbBlock.setOnClickListener(new BlockUnblockCardCheckedChangeListener());
        mTgbMerchant.setOnCheckedChangeListener(new ReasonOnCheckedChangeListener(mTxvMerchant));
        mTgbCreditCard.setOnCheckedChangeListener(new ReasonOnCheckedChangeListener(mTxvCreditCard));
        btnCancel.setOnClickListener(ActivityUtil.createCloseApplicatonViewOnClickListener(this));
        mBtnRefute.setOnClickListener(new RefuteOnClickListener());

        // setup default behavior of the UI
        mTgbBlock.setChecked(false);
        mTxvCardStatus.setText(getString(R.string.txb_card_unblocked));
        mTgbMerchant.setChecked(false);
        mTgbCreditCard.setChecked(false);

        // fetch Chargeback URL to fill in this UI
        Intent intent = getIntent();
        String chargebackURL = intent.getStringExtra(NoticeActivity.BUNDLE_CHARGEBACK_URL);

        // call URL to get data to fill in this UI and make it so by means of the post execution method
        GetChargebackAsyncTask getChargebackAsyncTask = new GetChargebackAsyncTask(this, getChargePostExecutor);
        getChargebackAsyncTask.execute(chargebackURL);
    }

    /**
     * This action is executed when this activity is being loaded, after an initial service is called.
     * It fetches data  to fulfill this user interface, and set the ooption to block the credit card
     * depending o a autoblock flag element.
     */
    public class GetChargeBackPostExecutor implements PostExecution<ChargebackHolder> {

        @Override
        public void populateUI(ChargebackHolder value) {
            if (value != null) {

                // store data in this class
                chargeback = value;
                mChargebackURL = chargeback.getLinks().getSelf().getHref();

                // make UI visible
                mLilMain.setBackgroundColor(ContextCompat.getColor(ChargebackActivity.this, R.color.colorBlack));
                mTxvTitle.setVisibility(View.VISIBLE);
                mRelForm.setVisibility(View.VISIBLE);
                mEdtComments.setVisibility(View.VISIBLE);
                mBtnRefute.setVisibility(View.VISIBLE);

                // fill in UI data
                mTxvTitle.setText(chargeback.getTitle());
                mEdtComments.setHint(Html.fromHtml(chargeback.getComment_hint()));
                mTxvMerchant.setText(chargeback.getReason_details()[REASON_MERCHANT_INDEX].getTitle());
                mTxvCreditCard.setText(chargeback.getReason_details()[REASON_CARD_INDEX].getTitle());
                mReasonsId[REASON_MERCHANT_INDEX] = chargeback.getReason_details()[REASON_MERCHANT_INDEX].getId();
                mReasonsId[REASON_CARD_INDEX] = chargeback.getReason_details()[REASON_CARD_INDEX].getId();

                // if necessary, block card
                if (chargeback.getAutoblock().equalsIgnoreCase(Boolean.TRUE.toString())) {
                    PostCardOperationAsyncTask blockCardAsyncTask = new PostCardOperationAsyncTask(ChargebackActivity.this, postChargeBackAutoBlockPostExec);
                    blockCardAsyncTask.execute(chargeback.getLinks().getBlock_card().getHref());
                }
            } else {
                // show error message
                Log.e(LOG_TAG, "An error has occurred fetching Chargeback data.");
                ActivityUtil.showUnexpectedErrorMessage(ChargebackActivity.this);
            }
        }
    }

    /**
     * This event is executed after the credit card is blocked. it changes the interface
     * based on the successful of the blocking operation.
     */
    public class PostChargebackBlockPostExecutor implements PostExecution<Boolean> {

        @Override
        public void populateUI(Boolean blockOperation) {
            if (blockOperation != null && blockOperation) {
                // handle UI for blocking card
                mTgbBlock.setChecked(true);
                mTxvCardStatus.setText(getString(R.string.txb_card_blocked));
            } else {
                // show error
                Log.e(LOG_TAG, "Error while locking or unlocking a card.");
                ActivityUtil.showUnexpectedErrorMessage(ChargebackActivity.this);
            }
        }
    }

    /**
     * This action is executed after the automatic blocking is executed. When run, this
     * event enables automatically reason items.
     */
    public class PostChargebackAutoBlockPostExecutor implements PostExecution<Boolean> {

        @Override
        public void populateUI(Boolean blockOperation) {
            if (blockOperation != null && blockOperation) {
                // handle UI for blocking card
                mTgbBlock.setChecked(true);
                mTxvCardStatus.setText(getString(R.string.txb_card_blocked_first_time));
                mTgbMerchant.setChecked(true);
                mTgbCreditCard.setChecked(true);
            } else {
                // show error
                Log.e(LOG_TAG, "Error while locking or unlocking a card.");
                ActivityUtil.showUnexpectedErrorMessage(ChargebackActivity.this);
            }
        }
    }

    /**
     * This action is executed after the credit card is unblocked. It makes the
     * necessary changes in the UI if the unblocking is executed successfully.
     */
    public class PostChargebackUnblockPostExecutor implements PostExecution<Boolean> {

        @Override
        public void populateUI(Boolean blockOperation) {
            if (blockOperation != null && blockOperation) {
                // handle UI for blocking card
                mTgbBlock.setChecked(false);
                mTxvCardStatus.setText(getString(R.string.txb_card_unblocked));
            } else {
                // show error
                Log.e(LOG_TAG, "Error while locking or unlocking a card.");
                ActivityUtil.showUnexpectedErrorMessage(ChargebackActivity.this);
            }
        }
    }

    /**
     * Whenever a Reason toggle button has its value changes, this listener is called
     * and makes some changes in the UI. Basically it adjust the toogle layout and its
     * text color.
     */
    private class ReasonOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {

        TextView relatedTextView = null;

        // padding settings
        private static final int PADDING_DEFAULT = 0;
        private static final int PADDING_ADJUSTED = 15;

        public ReasonOnCheckedChangeListener(TextView relatedTextView) {
            this.relatedTextView = relatedTextView;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                buttonView.setGravity(Gravity.LEFT | Gravity.CENTER);
                buttonView.setPadding(PADDING_ADJUSTED, PADDING_DEFAULT, PADDING_DEFAULT, PADDING_DEFAULT);
                relatedTextView.setTextColor(ContextCompat.getColor(ChargebackActivity.this, R.color.colorGreen));
            } else {
                buttonView.setGravity(Gravity.RIGHT | Gravity.CENTER);
                buttonView.setPadding(PADDING_DEFAULT, PADDING_DEFAULT, PADDING_ADJUSTED, PADDING_DEFAULT);
                relatedTextView.setTextColor(ContextCompat.getColor(ChargebackActivity.this, R.color.colorBlack));
            }
        }
    }

    /**
     * Whenever a click in the block/unblock toggle button is performed this listener is called.
     * It calls the respective service for blocking and unblocking the credit card. UI operations
     * are perfromed after their asynchronous events are performed.
     */
    private class BlockUnblockCardCheckedChangeListener implements CompoundButton.OnClickListener {

        @Override
        public void onClick(View v) {
            if (mTgbBlock.isChecked()) {
                // block card
                PostCardOperationAsyncTask blockCardAsyncTask = new PostCardOperationAsyncTask(ChargebackActivity.this, postChargeBackBlockPostExec);
                blockCardAsyncTask.execute(chargeback.getLinks().getBlock_card().getHref());
            } else {
                // unblock card
                PostCardOperationAsyncTask blockCardAsyncTask = new PostCardOperationAsyncTask(ChargebackActivity.this, postChargeBackUnblockPostExec);
                blockCardAsyncTask.execute(chargeback.getLinks().getUnblock_card().getHref());
            }
        }
    }

    /**
     * Event called when the refutal of the credit card button is clicked. It calls the service
     * for this operation sending data collected by this UI.
     */
    private class RefuteOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // fetch data from form
            nubank.vo.chargebackpost.Reason_details[] details = new nubank.vo.chargebackpost.Reason_details[REASON_ARRAY_SIZE];
            for (int index = 0; index < mReasonsId.length; index++) {
                details[index] = new nubank.vo.chargebackpost.Reason_details();
                details[index].setId(mReasonsId[index]);
            }
            details[REASON_MERCHANT_INDEX].setResponse(Boolean.valueOf(mTgbMerchant.isChecked()).toString());
            details[REASON_CARD_INDEX].setResponse(Boolean.valueOf(mTgbCreditCard.isChecked()).toString());

            ChargebackPost chargebackPost = new ChargebackPost();
            chargebackPost.setComment(mEdtComments.getText().toString());
            chargebackPost.setReason_details(details);

            // post chargeback
            PostChargebackAsyncTask postChargebackAsyncTask = new PostChargebackAsyncTask(ChargebackActivity.this, chargebackPost);
            postChargebackAsyncTask.execute(mChargebackURL);
        }
    }
}
