package nubank.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;

import nubank.asynctasks.GetRootAsyncTask;
import nubank.asynctasks.PostExecution;
import nubank.util.ActivityUtil;
import nubank.vo.notice.Notice;

/**
 * This is the starting User Interface which shows some information the user to read,
 * and gives him the option of continue with the payment cancelling operation, or to
 * quit the application.
 */
public class NoticeActivity extends Activity {

    private static final String LOG_TAG = NoticeActivity.class.getName();

    public static final String BUNDLE_CHARGEBACK_URL = "chargebackURL";

    // UI elements
    private LinearLayout mLilMain = null;
    private WebView mWevText = null;
    private Button mBtnPrimaryAction = null;
    private Button mBtnSecundaryAction = null;

    private String chargebackURL = null;

    private PopulateNotice populateNotice = new PopulateNotice();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        // fetch UI elements
        mLilMain = (LinearLayout) findViewById(R.id.lilMain);
        mWevText = (WebView) findViewById(R.id.wevText);
        mBtnPrimaryAction = (Button) findViewById(R.id.btnPrimaryAction);
        mBtnSecundaryAction = (Button) findViewById(R.id.btnSecundaryAction);

        // add listeners
        mBtnPrimaryAction.setOnClickListener(new ContinueOnClickListener());
        mBtnSecundaryAction.setOnClickListener(ActivityUtil.createCloseApplicatonViewOnClickListener(this));

        GetRootAsyncTask getRootAsyncTask = new GetRootAsyncTask(this, populateNotice);
        getRootAsyncTask.execute();
    }

    /**
     * This event is called after the click to continue the cancelling operation is performed.
     * Here the user is taken to the next screen with the URL necessary for populate the next UI.
     */
    private class ContinueOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent cbActivityIntent = new Intent(NoticeActivity.this, ChargebackActivity.class);
            cbActivityIntent.putExtra(BUNDLE_CHARGEBACK_URL, chargebackURL);
            startActivity(cbActivityIntent);
        }
    }

    /**
     * This event is called after the service for fetching data to this UI is called.
     * Here the screen is fulfilled with information retreived from the service which
     * called this event.
     */
    private class PopulateNotice implements PostExecution<Notice> {

        @Override
        public void populateUI(Notice notice) {
            if (notice != null) {
                // make everything visible
                mLilMain.setBackgroundColor(ContextCompat.getColor(NoticeActivity.this, R.color.colorBlack));
                mWevText.setVisibility(View.VISIBLE);
                mBtnPrimaryAction.setVisibility(View.VISIBLE);
                // setup text with web view
                String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + ("<h1>" + notice.getTitle().toString() + "</h1>" + notice.getDescription().toString()).toString();
                mWevText.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html; charset=UTF-8", "UTF-8", null);
                mWevText.setBackgroundColor(ContextCompat.getColor(NoticeActivity.this, R.color.colorBackground));
                // set appropriate text
                mBtnPrimaryAction.setText(notice.getPrimary_action().getTitle());
                mBtnSecundaryAction.setText(notice.getSecondary_action().getTitle());
                // store chargeback URL and enable continue button
                chargebackURL = notice.getLinks().getChargeback().getHref();
                mBtnPrimaryAction.setEnabled(true);
            } else {
                // show error message
                Log.e(LOG_TAG, "Problmes fetching data to populate the UI.");
                ActivityUtil.showUnexpectedErrorMessage(NoticeActivity.this);
            }
        }
    }
}
